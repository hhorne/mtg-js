The Active Player starts with priority.

If a player has priority when they cast a spell, activate an ability, 
or take a special action, that player receives priority afterward.

If a player has priority and chooses not to take any actions, that player
passes. Then the next player in turn order receives priority.

If all players pass in succession, the spell or ability on top of the stack
resolves or, if the stack is empty, the phase or step ends.

Active player receives priority after a spell or ability resolves