const inquirer = require('inquirer')
const PriorityActions = require('./priority-actions')
const { Pass } = PriorityActions

/**
 * Allows the player to respond to being passed priority.
 * 
 * @param {string} playerId The playerId of the player receiving priority
 * @param {Object} game State of the game when player receives priority
 */
async function handlePriority(playerId, game) {
  const choices = getPriorityChoices(playerId, game)
  const choice = await inquirer.prompt([{
    type: 'list',
    name: 'type',
    message: 'Actions: ',
    choices,
  }])

  return {
    action: () => { console.log('an effect put on the stack') },
    type: choice.type,
  };
}

function getPriorityChoices(playerId, game) {
  const { activePlayer } = game
  const choices = activePlayer === playerId
    ? getActivePlayerChoices(game)
    : getRespondingPlayerChoices(game)

  return choices
}

function getActivePlayerChoices(game) {
  const { turnEvent } = game
  const choices = []
  switch (turnEvent) {
    default:
      choices.push({ name: Pass.name, value: Pass, short: Pass.name })
  }

  return choices
}

function getRespondingPlayerChoices(game) {
  const { turnEvent } = game
  const choices = []
  switch (turnEvent) {
    default:
      choices.push({ name: Pass.name, value: Pass, short: Pass.name })
  }

  return choices
}

module.exports = handlePriority