const { Pass } = require('../priority-actions')

module.exports = jest.fn(() => ({ type: Pass }))