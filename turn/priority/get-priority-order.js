/**
 * Gets the order of player priority, based on the currently active players
 * position in turn order.
 * 
 * @param {(string|Array)} turnOrder Array of playerIds
 * @param {string} activePlayer Represents playerId of the Active Player
*/
function getPriorityOrder(turnOrder, activePlayer) {
  const startIndex = turnOrder.indexOf(activePlayer)
  const priorityOrder = [
    ...turnOrder.slice(startIndex),
    ...turnOrder.slice(0, startIndex),
  ]

  return priorityOrder
}

module.exports = getPriorityOrder