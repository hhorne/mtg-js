const getPriorityOrder = require('./get-priority-order')
const handlePriority = require('./handle-priority')
const PriorityActions = require('./priority-actions')
const { Pass: { id: passId } } = PriorityActions

/**
 * This will loop through the players giving them priority, starting with
 * the Currently Active player. If a player takes an action other than pass,
 * they will receive priority again immediately after. If all players pass
 * in succession, the function finishes.
 * 
 * @param {Object} game State of the game during this priority exchange
 */
async function exchangePriority(game) {
  const { activePlayer, players, turnOrder } = game
  const priorityOrder = getPriorityOrder(turnOrder, activePlayer)
  const playerCount = turnOrder.length
  let results = turnOrder.map(p => false)
  let index = 0
  let priorityCount = 0

  while (results.some(pass => pass === false)){
    index = (index + playerCount) % playerCount
    const playerId = priorityOrder[index]
    const player = players[playerId]

    console.log(`${player.name} has taken priority`)

    const result = await handlePriority(playerId, game)
    const passed = result.type.id === passId
    const takingAction = !passed
    
    results[index] = passed

    if (takingAction) {
      game.stack.push(result.action)
      results = results.map(p => false)
    } else {
      console.log(`${player.name} has passed priority`)
      index++
    }

    priorityCount++
  }
}

module.exports = exchangePriority
