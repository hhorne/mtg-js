/**
 * Checks the stack and resolves the spell on top
 * 
 * @param {Object} game State of the game when resolving the stack
 */
function resolveStack(game) {
  if (!game.stack.length) {
    return
  }

  game.stack.pop()
}

module.exports = resolveStack