const resolveStack = require('./resolve-stack')
const createTestGame = require('../../test/create-test-game')

describe('Resolve Stack', () => {

  it('Should pop the top item off the stack', () => {
    const game = createTestGame()
    game.stack = [{ id: 1 }, { id: 2 }, { id: 3 }]

    resolveStack(game)

    expect(game.stack.length).toBe(2)
    expect(!!game.stack.find(x => x.id === 3)).toBeFalsy()
  })

})