const exchangePriority = require('./exchange-priority')
const resolveStack = require('./resolve-stack')

/**
 * Handles the chain of priority and resolving spells played
 * when a player has priority.
 * 
 * @param {*} game State of the game at the start of priority
 */
async function priority(game) {
  const { players, stack } = game

  do {
    await exchangePriority(game)
    resolveStack(game)
  } while (stack.length)
}

module.exports = priority