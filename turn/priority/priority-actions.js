module.exports = {
  // None doesn't represent a real action players can take.
  // It's a stand in for use when you don't want to match any
  // actual priority action.
  None: {
    id: 'none',
    name: 'None',
  },

  Pass: {
    id: 'pass',
    name: 'Pass',
  },

  Cast: {
    id: 'cast',
    name: 'Cast',
  },

  SpecialAbility: {
    id: 'special-ability',
    name: 'Special Ability',
  },

}