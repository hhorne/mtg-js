const exchangePriority = require('./exchange-priority')
const createTestGame = require('../../test/create-test-game')
const handlePriority = require('./handle-priority')
const PriorityActions = require('./priority-actions')
const { Cast, Pass } = PriorityActions

jest.mock('./handle-priority')

describe('Exchange Priority', () => {
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('finishes if all players pass', async () => {
    const game = createTestGame()

    await exchangePriority(game)

    expect(handlePriority).toHaveBeenCalledTimes(2)
  })

  it('finshes on: cast, no response', async () => {
    handlePriority.mockReturnValueOnce({ type: Cast })

    const game = createTestGame()

    await exchangePriority(game)

    expect(handlePriority)
      .toHaveBeenCalledTimes(3)
  })

  it('finishes on: pass, cast, no response', async () => {
    handlePriority
      .mockReturnValueOnce({ type: Pass })
      .mockReturnValueOnce({ type: Cast })

      const game = createTestGame()

      await exchangePriority(game)

      expect(handlePriority)
        .toHaveBeenCalledTimes(4)
  })
})