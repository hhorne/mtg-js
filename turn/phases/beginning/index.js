const UntapStep = require('./untap-step')
const UpkeepStep = require('./upkeep-step')
const DrawStep = require('./draw-step')

const id = 'BeginningPhase'
const name = 'Beginning Phase'
const steps = new Map([
  [UntapStep.id, UntapStep],
  [UpkeepStep.id, UpkeepStep],
  [DrawStep.id, DrawStep],
])

module.exports = {
  id,
  name,
  steps,
}