const id = 'BeginningPhase/UntapStep'
const name = 'Untap Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    const { activePlayer, players } = game
    let { board: { permanents } } = players[activePlayer]

    // active player chooses which permanents are untapped
    // presumably all unless an effect limits it
    tappedPermanents = permanents.filter(p => p.tapped)

    return { done: false }
  },
}