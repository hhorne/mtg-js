const priority = require('../../priority')

const id = 'BeginningPhase/UpkeepStep'
const name = 'Upkeep Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    const { activePlayer, players } = game
    let { board: { permanents } } = players[activePlayer]

    await priority(game)

    permanentsWithUpkeep = permanents.filter(p => !!p.upkeep)

    return { done: false }
  },
}