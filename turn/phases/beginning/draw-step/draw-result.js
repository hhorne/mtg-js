const ResultTypes = require('./result-types')

const DrawResult = (card) => ({
  done: false,
  type: ResultTypes.Draw,
  value: card,
  toString: function() {
    const { value } = this
    return `Drew ${value.name}`
  }
})

module.exports = DrawResult