const DrawStep = require('./draw-step')
const createTestGame = require('../../../../test/create-test-game')
const handlePriority = require('../../../priority/handle-priority')

jest.mock('../../../priority/handle-priority')

describe('Draw Step', () => {
  
  describe('When Active Player has cards in library', () => {

    it('Should add cards to active players hand', async () => {
      const game = createTestGame()
      const { activePlayer, players } = game
      const { hand, library } = players[activePlayer]

      const drawStepResult = await DrawStep.action(game)
      
      expect(hand.length).toBe(1)
    })

    it('Should return a DrawResult when a card is drawn', async () => {
      const game = createTestGame()
      const { activePlayer, players } = game
      const player = players[activePlayer]
      const testCard = { name: 'Savannah Lions', manaCost: ['w'], type: ['creature', 'lions'], spell: true, }
      player.library[0] = testCard

      const drawResult = await DrawStep.action(game)

      expect (drawResult.toString()).toBe(`Drew ${drawResult.value.name}`)
    })

  })

  describe('When Active Players library is empty', () => {

    it('Should return `done`', async () => {
      const game = createTestGame()
      const { activePlayer, players } = game
      const player = players[activePlayer]
      player.library = []

      const drawStepResult = await DrawStep.action(game)

      expect(drawStepResult.done).toBeTruthy()
    })

  })

})