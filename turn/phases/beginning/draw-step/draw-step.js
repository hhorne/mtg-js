const priority = require('../../../priority')
const DrawResult = require('./draw-result')
const OutOfCardsResult = require('./out-of-cards-result')

const id = 'BeginningPhase/DrawStep'
const name = 'Draw Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    const { activePlayer, players } = game
    const { hand, library, name: playerName } = players[activePlayer]

    if (library.length === 0) {
      return OutOfCardsResult(playerName)
    }

    const card = library.shift();
    hand.push(card)

    await priority(game)

    return DrawResult(card)
  },
}