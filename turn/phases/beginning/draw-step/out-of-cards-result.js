const ResultTypes = require('./result-types')

const OutOfCardsResult = (playerName) => ({
  done: true,
  type: ResultTypes.OutOfCards,
  value: `${playerName} out of cards to draw`,
})

module.exports = OutOfCardsResult