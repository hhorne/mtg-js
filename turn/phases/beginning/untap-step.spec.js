const untapStep = require('./untap-step')
const createTestGame = require('../../../test/create-test-game')

describe('Untap Step', () => {
  it('Should untap all Active Players permanents', () => {
    const game = createTestGame()
    const { activePlayer, players } = game
    const player = players[activePlayer]

    const untapStepResult = untapStep.action(game)

    expect(player.board.permanents.every(p => !p.tapped)).toBeTruthy()
  })
})