const priority = require('../priority')

const id = 'MainPhase/Pre-Combat'
const name = 'Main Phase (Pre-Combat)'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
  }
}