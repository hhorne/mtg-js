const priority = require('../../priority')

const id = 'CombatPhase/BeginningStep'
const name = 'Beginning of Combat Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
	},
}