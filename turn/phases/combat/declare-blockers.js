const priority = require('../../priority')

const id = 'CombatPhase/DeclareBlockersStep'
const name = 'Declare Blockers Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
	},
}