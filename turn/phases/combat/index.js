const BeginningStep = require('./beginning')
const DeclareAttackersStep = require('./declare-attackers')
const DeclareBlockersStep = require('./declare-blockers')
const DamageStep = require('./damage')
const EndStep = require('./end')

const id = 'CombatPhase'
const name = 'Combat Phase'
const steps = new Map([
  [BeginningStep.id, BeginningStep],
  [DeclareAttackersStep.id, DeclareAttackersStep],
  [DeclareBlockersStep.id, DeclareBlockersStep],
  [DeclareBlockersStep.id, DeclareBlockersStep],
  [EndStep.id, EndStep],
])

module.exports = {
  id,
  name,
  steps,
}