const priority = require('../../priority')

const id = 'CombatPhase/CombatDamageStep'
const name = 'Combat Damage Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
	},
}