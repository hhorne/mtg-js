const priority = require('../../priority')

const id = 'CombatPhase/EndStep'
const name = 'End of Combat Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
	},
}
