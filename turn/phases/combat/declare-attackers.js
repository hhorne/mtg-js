const priority = require('../../priority')

const id = 'CombatPhase/DeclareAttackersStep'
const name = 'Declare Attackers Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
	},
}