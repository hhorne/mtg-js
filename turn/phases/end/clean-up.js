const priority = require('../../priority')

const id = 'EndingPhase/CleanupStep'
const name = 'Cleanup Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
  }
}