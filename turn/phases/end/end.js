const priority = require('../../priority')

const id = 'EndingPhase/EndStep'
const name = 'End Step'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
  }
}