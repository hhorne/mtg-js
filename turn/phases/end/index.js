const EndStep = require('./end')
const CleanUpStep = require('./clean-up')

const id = 'EndingPhase'
const name = 'Ending Phase'
const steps = new Map([
  [EndStep.id, EndStep],
  [CleanUpStep.id, CleanUpStep],
])

module.exports = {
  id,
  name,
  steps,
}