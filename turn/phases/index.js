const BeginningPhase = require('./beginning')
const MainPreCombatPhase = require('./main-pre-combat')
const CombatPhase = require('./combat')
const MainPostCombatPhase = require('./main-pre-combat')
const EndPhase = require('./end')

const TurnPhases = new Map([
  [BeginningPhase.id, BeginningPhase],
  [MainPreCombatPhase.id, MainPreCombatPhase],
  [CombatPhase.id, CombatPhase],
  [MainPostCombatPhase.id, MainPostCombatPhase],
  [EndPhase.id, EndPhase],
])

module.exports = TurnPhases