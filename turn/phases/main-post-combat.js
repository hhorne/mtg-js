const priority = require('../priority')

const id = 'MainPhase/Post-Combat'
const name = 'Main Phase (Post-Combat)'

module.exports = {
  id,
  name,
  action: async function (game) {
    await priority(game)
    return { done: false }
  }
}