const turn = require('./turn')
const createTestGame = require('../test/create-test-game')

describe('Turn', () => {

  it('Should make playerId the Active Player', () => {
    const game = createTestGame()

    turn('player2', game)

    expect(game.activePlayer).toBe('player2')
  })

})