const TurnPhases = require('./phases')

module.exports = async function turn(playerId, game) {
  const { players } = game
  const player = players[playerId]
  game.activePlayer = playerId

  console.log(`${player.name} has begun their turn.`)

  for (const [phaseKey, phase] of TurnPhases){
    console.log(`${phase.name}`)
    game.turnEvent = phase.id
    if (phase.action) {
      const phaseResult = await phase.action(game)

      if (phaseResult.done) {
        return phaseResult
      }
    }

    const { steps = new Map([]) } = phase
    for (const [stepKey, step] of steps) {
      console.log(`${step.name}`)
      game.turnEvent = step.id
      if (step.action) {
        const stepResult = await step.action(game)

        if (stepResult.done) {
          return stepResult
        } else if (stepResult.value.toString) {
          console.log(stepResult.value.toString())
        }
      }
    }
  }

  return { done: false }
}