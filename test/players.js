// this just creates n test players, will be deleted eventually

const { redDeck, whiteDeck } = require('./decks')

const decks = [redDeck, whiteDeck]

const createPlayers = (num) => Array.from(
    new Array(num), 
    (val, index) => index
  )
  .map(n => ({
    id: `player${n + 1}`,
    name: `Player ${n + 1}`,
    deck: decks[n + decks.length % decks.length] 
  }))

module.exports = createPlayers(2).reduce((prev, curr) => ({ ...prev, [curr.id]: curr }), {})