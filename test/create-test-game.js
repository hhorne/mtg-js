const Game = require('../game')
const players = require('./players')

const turnOrder = ['player1', 'player2']

const createTestGame = () => ({
  ...new Game({
    gameType: 'duel',
    turnOrder,
    players,
  }),
  activePlayer: turnOrder[0],
})

module.exports = createTestGame