// just some fake deck stubs

const LightningBolt = {
  name: 'Lightning Bolt',
  manaCost: ['r'],
  type: ['instant'],
}

const MonsGoblinRaiders = {
  name: 'Mons\'s Goblin Raiders',
  manaCost: ['r'],
  type: ['creature', 'goblin'],
}
const SavannahLions = {
  name: 'Savannah Lions',
  manaCost: ['w'],
  type: ['creature', 'lions'],
}

const HealingSalve = {
  name: 'Healing Salve',
  manaCost: ['w'],
  type: ['instant'],
}

const MoxPearl = {
  name: 'Mox Pearl',
  type: ['artifact'],
  manaCost: [0],
}

const MoxRuby = {
  name: 'Mox Ruby',
  type: ['artifact'],
  manaCost: [0],
}

const Mountain = {
  name: 'Mountain',
  type: ['basic-land', 'land', 'mountain'],
}

const Plains = {
  name: 'Plains',
  type: ['basic-land', 'land', 'plains'],
}

module.exports = {
  redDeck: [
    LightningBolt,
    LightningBolt,
    LightningBolt,
    LightningBolt,
    MonsGoblinRaiders,
    MonsGoblinRaiders,
    MonsGoblinRaiders,
    MonsGoblinRaiders,
    MoxRuby,
    MoxRuby,
    MoxRuby,
    MoxRuby,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
    Mountain,
  ],
  whiteDeck: [
    SavannahLions,
    SavannahLions,
    SavannahLions,
    SavannahLions,
    HealingSalve,
    HealingSalve,
    HealingSalve,
    HealingSalve,
    MoxPearl,
    MoxPearl,
    MoxPearl,
    MoxPearl,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
    Plains,
  ]
}