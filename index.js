const Game = require('./game')
const players = require('./test/players')
const turn = require('./turn')

const turnOrder = Game.rollTurnOrder(players)

const game = new Game({
  gameType: 'duel',
  turnOrder,
  players,
})

game.begin()

const main = async (game) => {
  let results
  while (!game.over) {
    results = []

    for (const playerId of turnOrder) {
      let result = await turn(playerId, game)
      results.push(result)
      
      if (result.done) {
        game.over = result.done
        break
      }
    }
  }

  const result = results.find(r => r.done).value
  console.log(result)
}

main(game)