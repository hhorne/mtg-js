
const rollDie = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is exclusive and the minimum is inclusive
}

const rollForInitiative = (playerIds) => playerIds
  .map(playerId => ({ id: playerId, result: rollDie(1, 6) }))
  .sort((a, b) => b.result - a.result)

/**
 * Shuffles array in place. Fisher-Yates shuffleish?
 * @param {Array} a items An array containing the items.
 */
const shuffle = (a) => {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
      j = Math.floor(Math.random() * (i + 1));
      x = a[i];
      a[i] = a[j];
      a[j] = x;
  }
  return a;
}

module.exports = {
  rollDie,
  rollForInitiative,
  shuffle,
}