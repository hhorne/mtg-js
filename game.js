const chalk = require('chalk')
const Player = require('./Player')
const { rollForInitiative } = require('./util')

const DefaultHandSize = 7

module.exports = class Game {
  constructor({ gameType, players, turnOrder }) {
    this.activePlayer = turnOrder
    this.gameType = gameType
    this.turnOrder = turnOrder
    this.players = Object.keys(players)
      .reduce((prev, curr) => ({
        ...prev,
        [curr]: new Player(players[curr]),
    }), {})
    
    this.over = false
    this.prioritizedPlayer = null
    this.stack = []
  }

  static rollTurnOrder(players) {
    const diceRolls = rollForInitiative(Object.keys(players))
    const turnOrder = diceRolls.map(r => r.id)

    console.log(chalk.white.bgBlue(' Turn Order: '))
    turnOrder.forEach((playerId, index) => {
      console.log(chalk.black.bgWhite(`${index + 1}. ${players[playerId].name}`))
    })

    return turnOrder
  }

  begin() {
    this.turnOrder.forEach(playerId => {
      const player = this.players[playerId]
      const { hand, library } = player

      player.shuffleLibrary()

      for (let i = 0; i < DefaultHandSize; i++) {
        hand.push(library.shift())
      }
    })
  }
}