const { shuffle } = require('./util')

module.exports = class Player {
  constructor(player) {
    this.name = player.name
    this.hand = []
    this.library = player.deck || []
    this.life = 20
    this.turn = 0
    this.board = {
      permanents: [],
      graveyard: [],
      exile: [],
    }
  }

  shuffleLibrary() {
    this.library = shuffle(this.library)
  }
}